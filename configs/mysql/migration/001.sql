-- Add ":new" property to all existing documents.
-- This property is now used to track for a specific new document
INSERT INTO property (name, document, revision) SELECT ":new", id, 1 FROM document
ON DUPLICATE KEY UPDATE name = VALUES(name);